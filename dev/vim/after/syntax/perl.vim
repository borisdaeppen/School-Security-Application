" Support Syntax Highlight for 'method foo':
"
"   use Function::Parameters 'method'
"   method foo ($bar) { print $self->{bar} }
"
" 'method' as PerlMethod and the word after 'method' as PerlMethodname
syn keyword PerlMethod method nextgroup=PerlMethodname skipwhite
syn match PerlMethodname '\i\+' contained
" give some colors
hi link PerlMethodname perlFunctionName
hi link PerlMethod perlStatement
