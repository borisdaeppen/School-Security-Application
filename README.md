# School-Security-Application

Insanely inscure Client-Server-Application for training purposes in secure programming.

PLEASE DON'T submit patches closing security holes! They are on purpose.

### basic code overview

The scripts in the [code](code) folder _roughly_ follow the diagram shown below.

![code diagram](dev/dia.png)
