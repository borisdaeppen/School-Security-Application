package Fulla::Commands::List;

use v5.14;
use warnings;
use experimental 'switch';

use Fulla::Singleton;
use Fulla::System;
use Function::Parameters 'method';
use English; # exception accessible in $ARG instead of $_
use Try::Tiny;

my $system = Fulla::System->new();

method reply ( $option ) {

    my $cmdout = '';

    try {
        if ($option) {
            $cmdout = $system->bash("ls $option");
        }
        else {
            $cmdout = $system->bash("ls");
        }
    }
    catch {
        Fulla::Singleton->get_logger->error($ARG);
        $cmdout = 'error in command';
    };

    return $cmdout;

}

1;

__END__

=encoding UTF-8

=head1 NAME

C<Fulla::Commands::List> - processes the command I<list>. List application directory.

This module is part of the C<Fulla> project.

=head1 SYNOPSIS

All implementations in C<Fulla::Commands> must implement a C<reply()> method.
The arguments and return value of C<reply()> depend on the implementation.

 print Fulla::Commands::Login->reply();
 
=head1 METHODS
 
=head2 reply
 
First argument can be a string, containing any options supported by the C<ls> command on the server system.
Returns the C<stdout> from C<ls> on the system.

 print Fulla::Commands::List->reply('-l');
 
=head1 AUTHOR
 
© Boris Däppen, Biel, 2017
