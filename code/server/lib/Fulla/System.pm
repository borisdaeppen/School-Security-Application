package Fulla::System;

use v5.14;
use warnings;

use Function::Parameters 'method';

method new () { bless {}, $self }

method bash ($command) { return `$command` }

1;
