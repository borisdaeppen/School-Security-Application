# connection parameters
server='192.168.220.13'
user='root'

# ensure, that vmadmin can read/write log files
ssh $user@$server 'chown vmadmin:vmadmin /home/vmadmin/*fulla*'

# deploy updated files
rsync -tv  bin/fulla $user@$server:/usr/bin
rsync -tvr lib/*     $user@$server:/usr/share/perl5

# ensure correct permissions
ssh $user@$server 'chmod    755 /usr/bin/fulla;\
                   chmod -R 644 /usr/share/perl5/Fulla;\
                   chmod -R 644 /usr/share/perl5/HTML;\
                   chmod -R u=rwX,go=rX /usr/share/perl5/Fulla;\
                   chmod -R u=rwX,go=rX /usr/share/perl5/HTML'

# restart application
ssh $user@$server 'cd /home/vmadmin; pkill -F pid.fulla; /usr/bin/fulla daemon'

# ensure, that vmadmin can read/write log files
ssh $user@$server 'chown vmadmin:vmadmin /home/vmadmin/*fulla*'
